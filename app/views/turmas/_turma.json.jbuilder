json.extract! turma, :id, :nome, :nivel, :horario, :professor_id, :created_at, :updated_at
json.url turma_url(turma, format: :json)
