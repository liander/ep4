class AlunoController < ApplicationController
    def index
        authenticate_aluno!
    end
    def aluno_params
        params.require(:aluno).permit(:turma_id)
    end
    def show_all
        :authenticate_secretario!
        @alunos = Aluno.all
    end
end
