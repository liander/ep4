class SecretarioController < ApplicationController
  def index
    :authenticate_secretario!
  end
  def show_all
    :authenticate_secretario!
    @secretarios = Secretario.all
  end
end
