class ApplicationController < ActionController::Base

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:nome, :matricula])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:nome, :matricula, :formacao])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:nome, :matricula, :turma_id])
    devise_parameter_sanitizer.permit(:account_update, keys: [:nome, :matricula])
    devise_parameter_sanitizer.permit(:account_update, keys: [:nome, :matricula, :formacao])
    devise_parameter_sanitizer.permit(:account_update, keys: [:nome, :matricula, :turma_id])
  end
end
