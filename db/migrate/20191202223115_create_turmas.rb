class CreateTurmas < ActiveRecord::Migration[6.0]
  def change
    create_table :turmas do |t|
      t.string :nome
      t.integer :nivel
      t.text :horario
      t.references :professor, null: false, foreign_key: true

      t.timestamps
    end
  end
end
