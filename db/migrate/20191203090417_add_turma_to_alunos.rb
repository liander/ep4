class AddTurmaToAlunos < ActiveRecord::Migration[6.0]
  def change
    add_reference :alunos, :turma, null: true, foreign_key: {on_delete: :nullify}
  end
end
