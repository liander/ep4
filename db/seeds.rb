# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Secretario.create(email: 'liander@admin', password: 'password', nome: 'Liander', matricula: 18003715)
Secretario.create(email: 'carlos@admin', password: 'password', nome: 'Carlos', matricula: 00000000)
Professor.create(email: 'renatocoral@prof', password: 'password', nome: 'Renato Coral', matricula: 00000000, formacao: "Eng. Mecatrônica")
Professor.create(email: 'wreis@prof', password: 'password', nome: 'William Reis', matricula: 00000000, formacao: "Física")
Professor.create(email: 'liander@prof', password: 'password', nome: 'Liander', matricula: 18003715, formacao: "Eng. Aeroespacial")
Aluno.create(email: 'liander@aluno', password: 'password', nome: 'Liander', matricula: 18003715)
Aluno.create(email: 'carlos@aluno', password: 'password', nome: 'Carlos', matricula: 00000000)
Aluno.create(email: 'aluno1@aluno', password: 'password', nome: 'Fulano', matricula: 00000000)
Aluno.create(email: 'aluno2@aluno', password: 'password', nome: 'Ciclano', matricula: 00000000)



