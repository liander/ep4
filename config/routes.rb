Rails.application.routes.draw do
  resources :turmas

  devise_for :alunos, :controllers => {:registrations => "aluno/registrations", :sessions => "aluno/sessions" }
  devise_scope :alunos do
    get   'signup' => "aluno/registrations#new"
  end

  devise_for :professors, :controllers => {:registrations => "professor/registrations", :sessions => "professor/sessions" }
  devise_scope :professors do
    get   'signup' => "professor/registrations#new"
  end
  get 'secretario/signup' => "secretario/registrations#new"
  get 'secretarios', to: 'secretario#show_all'
  get 'professors', to: 'professor#show_all'
  get 'alunos', to: 'aluno#show_all'
  get 'secretario', to: 'secretario#index'
  get 'turmas', to: 'turmas#index'
  get 'professor', to: 'professor#index'
  get 'aluno', to: 'aluno#index'
  get 'aluno_historico', to: 'turmas#index'

  root 'home#inicio'
  namespace :secretario do
    root :to => "secretario#index"
  end
  namespace :professor do
    root :to => "professor#index"
  end
  namespace :aluno do
    root :to => "aluno#index"
  end

  devise_for :secretarios, :controllers => {:registrations => "secretario/registrations", :sessions => "secretario/sessions" }
  devise_scope :secretarios do
    get   'signup' => "secretario/registrations#new"
  end

  #devise_for :professors, :class_name => 'Professor', :controllers => {:registrations => "professor/professor" } do
   # get   "professor/sign_up" => "professor#new", :as => :private_customer_signup
  #end


  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
